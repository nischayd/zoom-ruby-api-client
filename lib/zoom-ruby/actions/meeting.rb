require 'zoom-ruby/utility'

module ZoomRuby
  module Actions
    module Meeting
      
      def meeting_create(*args)
        options = ZoomRuby::Utility.extract_options(args)
        ZoomRuby::Utility.parse_response self.class.post('/users/me/meetings', body: options.to_json, headers: self.get_auth_headers )
      end

      # def user_get(*args)
      #   options = ZoomRuby::Utility.extract_options!(args)
      #   Utils.require_params([:id], options)
      #   Utils.parse_response self.class.post('/user/get', :query => options)
      # end

      # ZoomRuby::Utility.define_bang_methods(self)

    end
  end
end
