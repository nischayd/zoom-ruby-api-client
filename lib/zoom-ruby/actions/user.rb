require 'zoom-ruby/utility'

module ZoomRuby
  module Actions
    module User
      
      def user_list(*args)
        options = ZoomRuby::Utility.extract_options(args)
        ZoomRuby::Utility.parse_response self.class.get('/users', query: options, headers: self.get_auth_headers )
      end

      # def user_get(*args)
      #   options = ZoomRuby::Utility.extract_options!(args)
      #   Utils.require_params([:id], options)
      #   Utils.parse_response self.class.post('/user/get', :query => options)
      # end

      # ZoomRuby::Utility.define_bang_methods(self)

    end
  end
end
