

require 'httparty'
require 'json'
require 'base64'
require 'openssl'
require 'jwt'
require 'zoom-ruby/actions/user'
require 'zoom-ruby/actions/meeting'

module ZoomRuby
  class Sdk
    include HTTParty
    include ZoomRuby::Actions::Meeting
    include ZoomRuby::Actions::User

    base_uri 'https://api.zoom.us/v2'

    def initialize(*args)
      options = ZoomRuby::Utility.extract_options(args)
      self.class.default_params(
        api_key: options[:api_key],
        api_secret: options[:api_secret]
      )
      self.class.default_timeout(options[:timeout])
    end

    def get_signature(options = {})
      time = (Time.current.to_i * 1000 - 30000).to_s
      data = Base64.strict_encode64(self.class.default_params[:api_key] + options[:meeting_number] + time + options[:role].to_s)
      hash = Base64.strict_encode64(OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha256'), self.class.default_params[:api_secret], data))
      tempStr = self.class.default_params[:api_key] + '.' + options[:meeting_number] + '.' + time + '.' + options[:role].to_s + '.' + hash
      return Base64.strict_encode64(tempStr).gsub('+', '-').gsub('/', '_').gsub(/#{Regexp.escape('=')}+$/, '')
    end

    def get_jwt_token
      payload = {
        "iss": self.class.default_params[:api_key].to_s,
        "exp": 1496091964000
      }
      JWT.encode payload, self.class.default_params[:api_secret].to_s, 'HS256'
    end

    def get_auth_headers
      {'Authorization': "Bearer #{self.get_jwt_token}", 'Content-Type' => 'application/json' }
    end 
  end
end
