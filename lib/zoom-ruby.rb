$:.unshift File.dirname(__FILE__)

require 'zoom-ruby/sdk'
require 'zoom-ruby/utility'
# require 'zoom-ruby/actions/user'
# require 'zoom-ruby/actions/meeting'

module ZoomRuby
  class << self
    attr_accessor :configuration

    def new
      @configuration ||= Configuration.new
      ZoomRuby::Sdk.new(
        api_key: ENV["ZOOM_API_KEY"],
        api_secret: ENV["ZOOM_API_SECRET"],
        timeout: 20,
      )
    end

    def configure
      @configuration ||= Configuration.new
      yield(@configuration)
    end
  end

  class Configuration
    attr_accessor :api_key, :api_secret, :timeout

    def initialize
      @api_key = @api_secret = '###'
      @timeout = 20
    end
  end
end